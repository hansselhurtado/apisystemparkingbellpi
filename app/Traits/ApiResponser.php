<?php

namespace App\Traits;

trait ApiResponser
{
	private function response($data, $code)
	{
		return response()->json(['data' => $data], $code);
	}

    protected function successResponse($data, $message = null, $code = 200)
	{
        return response()->json([
            'data'          => $data,
            'message'       => $message,
            'response'      => true,
            'code'          => $code,
        ]);
	}

	protected function errorResponse($message, $code)
	{
		return response()->json(['error' => $message, 'code' => $code], $code);
	}

    protected function showMessage($message, $code = 200, $response = true)
    {
        return response()->json([
            "messages"  => $message,
            "status"    => $code,
            "response"  => $response,
        ]);
    }
}
