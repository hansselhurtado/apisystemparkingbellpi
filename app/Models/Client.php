<?php

namespace App\Models;

use Database\Factories\ClientFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $table = 'clients';

    protected $fillable = ['id','name', 'last_name', 'status' ];

    static function client($id){
        $client =  Client::find($id);
        if ($client) {
           return $client;
        }
        return false;
    }

    public function Vehicle() {
        return $this->hasMany(Vehicle::class);
    }

    // usando factory
    protected static function newFactory(): ClientFactory {
        return ClientFactory::new();
    }
}
