<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    use HasFactory;

    protected $table = 'discounts';

    protected $fillable = ['name', 'minutes', 'discount','status' ];

    const NAME = ['Descuento de media hora','Super descuento de una hora'];

    const AVAILABLE     = '1';
    const NOT_AVAILABLE = '0';

    const DISCOUNT = [
        'Descuento de media hora'       => 10,
        'Super descuento de una hora'   => 20
    ];

    const MINUTES = [
        'Descuento de media hora'       => 30,
        'Super descuento de una hora'   => 60
    ];

    static function find_discount($minutes){
        foreach (Discount::all() as $value) {
            if($value->status == Discount::AVAILABLE){
                if($minutes >= $value->minutes) return true;
            }
        }
        return false;
    }

    static function value_discount($minutes){
        if(Discount::find_discount($minutes)){
            $num = 0;
            foreach (Discount::all() as $value) {
                if($value->status == Discount::AVAILABLE){
                    if($minutes >= $value->minutes){
                        if ($num <= $value->minutes) {
                            $num = $value->minutes;
                            $discount = $value;
                        }
                    }
                }
            }
            return $discount;
        }
        return false;
    }

    static function discount($state){
        return Discount::where('status',$state);
    }

    public function parkingRegister() {
        return $this->hasMany(ParkingRegister::class);
    }
}
