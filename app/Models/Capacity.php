<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Capacity extends Model
{
    use HasFactory;

    protected $table = 'capacity';

    protected $fillable = ['id_vehicle_type', 'capacity', 'available' ];

    // capacidad por vehículos
    const CAPACITY = [
        'Automóvil'     => 10,
        'Motocicleta'   => 20,
        'Bicicleta'     => 10
    ];

    static function capacity($id){
        return Capacity::where('id_vehicle_type',$id)->first();
    }

    public function vehicleType() {
        return $this->belongsTo(VehicleType::class,'id_vehicle_type');
    }
}
