<?php

namespace App\Models;

use Database\Factories\ParkingRegisterFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParkingRegister extends Model
{
    use HasFactory;

    protected $table = 'parking_registers';

    protected $fillable = ['amount', 'id_capacity', 'id_discount', 'id_vehicle','position','status' ];

    const ADENTRO   = '1';
    const SALIDA    = '0';

    static function find_car_by_position($id, $position){
        $register =  ParkingRegister::where('id_capacity', $id)
                                    ->where('status', ParkingRegister::ADENTRO)
                                    ->get();
        foreach ($register as $value) {
           if($value->position == $position){
               return true;
            }
        }
        return false;
    }

    static function find_vehicle_by_register($id){
        $register = ParkingRegister::where('id_vehicle', $id)
                                    ->where('status', ParkingRegister::ADENTRO)
                                    ->first();
        if ($register) {
           return true;
        }
        return false;
    }

    static function vehicle_by_registe($state){
        return ParkingRegister::where('status',$state);
    }

    static function register_by_type($id, $data){
        $register =  ParkingRegister::where('id_capacity',$id);

        if(isset($data['desde']) && isset($data['hasta'])){
            $register->whereDate('parking_registers.created_at', '>=', $data['desde'])
                    ->whereDate('parking_registers.created_at', '<=', $data['hasta']);
        }
        else{
            if(isset($data['desde'])){
                $register->whereDate('parking_registers.created_at', $data['desde']);
            }
            if(isset($data['hasta'])){
                $register->whereDate('parking_registers.created_at', $data['hasta']);
            }
        }
        return $register->get();
    }

    static function register_vehicle($id, $position){
        return ParkingRegister::where('id_capacity', $id)
                              ->where('status', ParkingRegister::ADENTRO)
                              ->where('position', $position)
                              ->first();
    }


    public function capacity() {
        return $this->belongsTo(Capacity::class,'id_capacity');
    }

    public function vehicle() {
        return $this->belongsTo(Vehicle::class,'id_vehicle');
    }

    public function discount() {
        return $this->belongsTo(Discount::class,'id_discount');
    }
}
