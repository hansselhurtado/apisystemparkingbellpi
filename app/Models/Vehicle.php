<?php

namespace App\Models;

use Database\Factories\VehicleFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    use HasFactory;

    protected $table = 'vehicles';

    protected $fillable = ['plate', 'brand', 'model','id_client','id_vehicle_type','status' ];

    public function client() {
        return $this->belongsTo(Client::class,'id_client');
    }

    public function vehicleType() {
        return $this->belongsTo(VehicleType::class,'id_vehicle_type');
    }

    public function ParkingRegister() {
        return $this->hasMany(ParkingRegister::class);
    }

    // usando factory
    protected static function newFactory(): VehicleFactory {
        return VehicleFactory::new();
    }
}
