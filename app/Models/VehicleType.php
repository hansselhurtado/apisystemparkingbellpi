<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VehicleType extends Model
{
    use HasFactory;

    protected $table = 'vehicle_types';

    protected $fillable = ['name', 'rate', 'status'];

    const NAME = ['Automóvil','Motocicleta','Bicicleta'];

    // tarifa por minutos
    const RATE = [
        'Automóvil'     => 150,
        'Motocicleta'   => 100,
        'Bicicleta'     => 50
    ];

    const VALUE = [
        'Automóvil'     => 1,
        'Motocicleta'   => 2,
        'Bicicleta'     => 3
    ];

    public function capacity() {
        return $this->hasOne(Capacity::class);
    }

    public function vehicle() {
        return $this->hasOne(Vehicle::class);
    }

}
