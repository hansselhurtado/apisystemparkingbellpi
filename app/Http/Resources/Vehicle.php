<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Vehicle extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'plate'             => $this->plate,
            'brand'             => $this->brand,
            'model'             => $this->model,
            'type_vehicle'      => $this->vehicleType->name,
            'client'            => $this->client->name.' '.$this->client->last_name,
            'docmuent'          => $this->client->id,
            'status'            => $this->status == '0' ? 'Desactivado':'Activo',
            'date'              => date_format($this->created_at, 'Y/m/d H:i:s'),
        ];
    }
}
