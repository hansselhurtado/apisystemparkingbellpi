<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class RegisterParking extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $admission  = Carbon::parse($this->created_at);
        $exit       = Carbon::parse($this->updated_at);
        $dif        = $admission->diffInMinutes($exit);

        return [
            'id'                        => $this->id,
            'name'                      => $this->vehicle->client->name.' '.$this->vehicle->client->last_name,
            'document'                  => $this->vehicle->client->id,
            'plate'                     => $this->vehicle->plate,
            'brand'                     => $this->vehicle->brand,
            'model'                     => $this->vehicle->model,
            'type_vehicle'              => $this->vehicle->vehicleType->name,
            'position'                  => $this->position,
            'amount'                    => number_format($this->amount),
            'discount'                  => $this->id_discount ? $this->discount->name: null,
            'discount_percentage'       => $this->id_discount ? $this->discount->discount: null,
            'admission'                 => date_format($this->created_at, 'Y/m/d H:i:s') ,
            'exit'                      => date_format($this->updated_at, 'Y/m/d H:i:s') ,
            'rate'                      => $this->vehicle->vehicleType->rate,
            'duration'                  => $dif.' minutos',
            'status'                    => $this->status == '0' ? 'Salida':'Adentro',
        ];
    }
}
