<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Discount extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'             => $this->id,
            'name'           => $this->name,
            'minutes'        => $this->minutes,
            'discount'       => $this->discount.'%',
            'status'         => $this->status == '1' ?'Activo':'Desactivado',
        ];
    }
}
