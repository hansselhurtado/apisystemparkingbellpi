<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator as ValidationValidator;
use Illuminate\Http\Exceptions\HttpResponseException;

class RegisterParkingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vehicle.id'                    => 'bail|required|exists:vehicles,id',
            // 'vehicle.position'              => 'min:1|max:20|numeric',
        ];
    }

    public function messages(){
        return  [
            'vehicle.id.required'                => 'El vehiculo es requerido',
            'vehicle.id.exists'                  => 'El vehiculo no existe',
            // 'vehicle.position.max'               => 'La posición no existe en el parqueadero',
            // 'vehicle.position.min'               => 'La posición no existe en el parqueadero',
            // 'vehicle.position.numeric'           => 'La posición debe ser un numero entero',
        ];
    }

    public function failedValidation(ValidationValidator $validator) {
        $message = $validator->errors()->first();
        throw new HttpResponseException(response()->json(['status' => 500,'messages' => $message]));
    }
}
