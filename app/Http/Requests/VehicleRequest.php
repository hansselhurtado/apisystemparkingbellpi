<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator as ValidationValidator;
use Illuminate\Http\Exceptions\HttpResponseException;


class VehicleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client.document'           => 'bail|required|min:3|max:10',
            'client.name'               => 'required|min:3',
            'client.last_name'          => 'required|min:3',
            'vehicle.plate'             => 'required|unique:vehicles,plate',
            'vehicle.brand'             => 'required|min:3',
            'vehicle.model'             => 'required|min:3',
            'vehicle.id_vehicle_type'   => 'required|exists:vehicle_types,id',
        ];
    }

    public function messages(){
        return  [
            'client.document.required'              => 'El documento es requerido',
            'client.document.min'                   => 'El documento debe tener minimo 3 numeros',
            'client.document.max'                   => 'El documento debe tener maximo 10 numeros',
            'client.name.required'                  => 'El nombre es requerido',
            'client.name.min'                       => 'El nombre debe tener minimo 3 caracteres',
            'client.last_name.required'             => 'El apellido es requerido',
            'client.last_name.min'                  => 'El apellido debe tener minimo 3 caracteres',
            'vehicle.plate.required'                => 'El placa es requerido',
            'vehicle.plate.unique'                  => 'La placa ya se encuentra registrada',
            'vehicle.brand.required'                => 'La marca es requerida',
            'vehicle.model.required'                => 'El modelo es requerido',
            'vehicle.id_vehicle_type.required'      => 'El tipo de vehiculo es requerido',
            'vehicle.id_vehicle_type.exists'        => 'El tipo de vehiculo no existe',
        ];
    }

    public function failedValidation(ValidationValidator $validator) {
        $message = $validator->errors()->first();
        throw new HttpResponseException(response()->json(['status' => 500,'messages' => $message]));
    }
}
