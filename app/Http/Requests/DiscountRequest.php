<?php

namespace App\Http\Requests;


use Illuminate\Contracts\Validation\Validator as ValidationValidator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class DiscountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'discount.name'                 => 'bail|required|unique:discounts,name',
            'discount.discount'             => 'required|min:1|max:50|numeric|unique:discounts,discount',
            'discount.minutes'              => 'required|min:1|numeric|unique:discounts,minutes',
        ];
    }

    public function messages(){
        return  [
            'discount.name.required'                 => 'El nombre es requerido',
            'discount.name.unique'                   => 'Este descuento ya existe con este nombre',
            'discount.discount.required'             => 'El valor del descuento es requerido',
            'discount.discount.max'                  => 'No puede crear un descuento con más de 50%',
            'discount.discount.numeric'              => 'El valor del descuento solo puede ser numero',
            'discount.discount.unique'               => 'Ya existe un descuento con este valor',
            'discount.minutes.required'              => 'El valor del minutos es requerido',
            'discount.minutes.numeric'               => 'El valor de minutos solo puede ser numero',
            'discount.minutes.unique'                =>  'Ya existe un descuento con este valor de minutos',
        ];
    }

    public function failedValidation(ValidationValidator $validator) {
        $message = $validator->errors()->first();
        throw new HttpResponseException(response()->json(['status' => 500,'messages' => $message]));
    }
}
