<?php

namespace App\Http\Controllers;

use App\Http\Requests\DiscountRequest;
use App\Http\Resources\Discount;
use App\Http\Resources\DiscountCollection;
use App\Models\Discount as ModelsDiscount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DiscountController extends ApiController
{
    protected $discount;

    public function __construct(ModelsDiscount $discount)
    {
        $this->discount = $discount;
    }

    public function index()
    {
        return $this->successResponse(new DiscountCollection($this->discount::all()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DiscountRequest $request)
    {
        try {
            DB::beginTransaction();

            $this->discount::create($request->discount);

            DB::commit();
            return $this->showMessage('descuento creado con existo');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 409);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function handle_status(Request $request,ModelsDiscount $discount)
    {
        $discount->update([
            "status" => $request->state,
        ]);
        return $this->showMessage('descuento actulizado');
    }

    public function destroy(ModelsDiscount $discount)
    {
       $discount->delete();
    }
}
