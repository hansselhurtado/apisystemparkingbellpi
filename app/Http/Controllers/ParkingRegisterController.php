<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterParkingRequest;
use App\Http\Resources\RegisterParking;
use App\Http\Resources\RegisterParkingCollection;
use App\Models\Capacity;
use App\Models\Discount;
use App\Models\ParkingRegister;
use App\Models\Vehicle;
use App\Models\VehicleType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ParkingRegisterController extends ApiController
{
    protected $parking;
    protected $capacity;
    protected $vehicleType;
    protected $vehicle;
    protected $discount;

    public function __construct(ParkingRegister $parking, Capacity $capacity, VehicleType $vehicleType, Vehicle $vehicle, Discount $discount){
        $this->parking      = $parking;
        $this->capacity     = $capacity;
        $this->vehicleType  = $vehicleType;
        $this->vehicle      = $vehicle;
        $this->discount     = $discount;
    }

    public function index()
    {
        return $this->successResponse(new RegisterParkingCollection($this->parking::vehicle_by_registe(ParkingRegister::ADENTRO)->get()));
    }

    // reporte de puesto que más se repita
    public function report_repeated(Request $request){
        $data   = [];
        $repeat = 0;
        $car    = $this->array_register($this->vehicleType::VALUE['Automóvil'], $request->all());
        $motor  = $this->array_register($this->vehicleType::VALUE['Motocicleta'], $request->all());
        $bici   = $this->array_register($this->vehicleType::VALUE['Bicicleta'], $request->all());

        array_push($data, $car);
        array_push($data, $motor);
        array_push($data, $bici);

        foreach ($data as $value) {
            if ($value['repeat'] > $repeat){
                $repeat     = $value['repeat'] ;
                $position   = $value['position'];
                $type       = $value['type'];
            }
        }

        if ($repeat > 0) {
            return response()->json([
                'position'      => $position,
                'repeat'        => $repeat,
                'type'          => $this->vehicleType::find($type)->name,
                'response'      => true,
            ]);
        }
        return $this->showMessage('Noy registro para esta fecha');
    }

    function array_register($id, $date) {
        $data       = [];
        $major      = 0;
        $position   = 0;
        foreach ($this->parking::register_by_type($id, $date) as $value){
            array_push($data, $value->position);
        };
        $data = array_count_values($data);
        foreach($data as $key => $q){
            if ($q > $major){
                $major      = $q;
                $position   = $key;
            }
        }
        return [
            'position'  => $position,
            'repeat'    => $major,
            'type'      => $id,
        ];
    }

    // reporte de transacciones de vehiculo
    public function report_vehicle(Request $request){
        $register =  $this->parking::vehicle_by_registe(ParkingRegister::SALIDA);
        $register =  $this->filter($request->all(), $register);
        return $this->successResponse(new RegisterParkingCollection($register->get()));
    }

    // reporte de valor por dia
    public function report_mount(Request $request){
        $data     =  [];
        $amount   =  0;
        $register =  $this->parking::vehicle_by_registe(ParkingRegister::SALIDA);
        $register =  $this->filter($request->all(), $register);
        $register =  $register->orderBy('created_at')->get();
        if($register->count()){
            $date     =  Carbon::parse($register[0]->created_at)->format('d-m-Y');
            foreach ($register as $value) {
                if(Carbon::parse($value->created_at)->format('d-m-Y') == $date){
                    $amount = $amount + $value->amount;
                }else{
                    $array = [
                        "day"    => $date,
                        "amount" => number_format($amount),
                    ];
                    array_push($data, $array);
                    $date   = Carbon::parse($value->created_at)->format('d-m-Y');
                    $amount = 0;
                    $amount = $amount + $value->amount;
                }
            }
            $array = [
                "day"    => $date,
                "amount" => number_format($amount),
            ];
            array_push($data, $array);
            return $this->successResponse($data);
        }
        return $this->showMessage('No hay registro para esta fecha',409,false);
    }


    function filter($data, $model){

        if (isset($data['tipo'])) {
            $model->where('id_capacity', $data['tipo']);
        }
        if(isset($data['desde']) && isset($data['hasta'])){
            $model->whereDate('created_at', '>=', $data['desde'])
                  ->whereDate('created_at', '<=', $data['hasta']);
        }else{
            if(isset($data['desde'])){
                $model->whereDate('created_at', $data['desde']);
            }
            if(isset($data['hasta'])){
                $model->whereDate('created_at', $data['hasta']);
            }
        }
        return $model;
    }

    public function store(RegisterParkingRequest $request)
    {
        try {
            DB::beginTransaction();

            $parking    = $request->vehicle;
            $vehicle    = $this->vehicle::find($parking['id']);
            $capacity   = $this->capacity::capacity($vehicle->id_vehicle_type);//conocer la disponibilidad en el parqueadero del tipo de vehículo

            if(!$this->parking::find_vehicle_by_register($parking['id'])){//verificar si el vehiculo se encuentra parqueado
                if($capacity->available > 0){
                    if(isset($parking['position'])){
                        if($parking['position'] <= $capacity->capacity ){
                            if(!$this->parking::find_car_by_position($capacity->id, $parking['position'])){//buscar si la posición se encuentra disponible
                                $this->save_parking($capacity, $parking);//guardamos el registro del parqueadero
                            }else{
                                return $this->showMessage('Esta posición no se encuentra disponible');
                            }
                        }else{
                            return $this->showMessage('Esta posición no existe para este tipo de vehiculo');
                        }
                    }else{
                        $this->save_without_position($capacity, $parking);//guardamos el registro de parqueo, calculando la posición
                    }
                }else{
                    return $this->showMessage('No hay disponibilidad en este momento', 503);
                }
            }else{
                return $this->showMessage('El vehículo se encuentra actualmente parqueado',500);
            }

            DB::commit();
            return $this->showMessage('Se ha guardado el parqueo');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 409);
        }
    }

    function save_without_position($capacity, $parking){
        if ($capacity->id_vehicle_type == VehicleType::VALUE['Bicicleta']) {
            for ($i=$capacity->capacity; $i > 0 ; $i--) {
                if(!$this->parking::find_car_by_position($capacity->id, $i)){
                    $position = $i;
                    break;
                }
            }
        }else{
            $sw     = false;
            $top    = $capacity->capacity;
            $medium = $capacity->capacity/2;

            while (!$sw) {
                for ($i=$top; $i > $medium ; $i--) {
                    if(!$this->parking::find_car_by_position($capacity->id, $i)){
                        $sw       = true;
                        $position = $i;
                        break;
                    }
                }
                $top    = $medium;
                $medium = 0;
            }
        }
        $parking['position'] = $position;
        $this->save_parking($capacity, $parking);
    }

    function save_parking($capacity, $parking){
        $parking['id_capacity'] = $capacity->id;
        $parking['id_vehicle']  = $parking['id'];
        $this->parking::create($parking);
        $this->update_capacity($capacity, '-');
    }

    function update_capacity($capacity, string $add){
        if($add == '-'){
            $available  = [
                'available' => $capacity->available - 1,
            ];
        }else{
            $available  = [
                'available' => $capacity->available + 1,
            ];
        }
        $capacity->update($available);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    // retirar un vehiculo del parqueadero
    public function update(Request $request, $id)
    {
        $register = $this->parking::find($id);
        if ($register && $register->status != '0') {
            $capacity   = $this->capacity::capacity($register->vehicle->id_vehicle_type);
            $register->update(["status" => $this->parking::SALIDA]);//actualizamos el estado del registro
            $this->update_capacity($capacity, '+');//actulizamos la disponibilidad del parqueadero
            $register   = $this->add_amount($register);//calcular el precio
            return $this->successResponse(new RegisterParking($register));
        }else{
            return $this->errorResponse('No existe ningún registro o ya ha salido del parqueadero', 409);
        }
    }

    function add_amount($register){
        // calcular cuanto se demoró
        $admission  = Carbon::parse($register->created_at);
        $exit       = Carbon::parse($register->updated_at);
        $dif        = $admission->diffInMinutes($exit);//diferencia en minutos
        $rate       = $register->vehicle->vehicleType->rate;
        $value      = $dif * $rate;//valor del servicio
        // establecer descuento
        $discount = $this->discount::value_discount($dif);
        if($discount){//verificamos si se le aplica algun descuento
            $value  = $value - ($value * ($discount->discoun/100));
            $data   = [
                "amount"        => $value,
                "id_discount"   => $discount->id
            ];
        }else{
            $data   = [
                "amount" => $value
            ];
        }
        $register->update($data);
        $this->check_position($register);//verificar si no tiene vehiculos adelante para poder salir
        return $register;
    }

    function check_position($register){
        if($register->capacity->id_vehicle_type != $this->vehicleType::VALUE['Bicicleta'] ){//si es bicicleta no se verifica la posición
            $position   = $register->position;
            $available  = $register->capacity;
            $mediun     = $register->capacity->id_vehicle_type == $this->vehicleType::VALUE['Automóvil'] ? $position - 5:  $position - 10;//posición delante de el vehiculo
            if($this->parking::find_car_by_position($register->id_capacity, $mediun)){//verificar si hay vehiculo delante de el
                $vehicle = $this->parking::register_vehicle($available->id, $mediun);//traer el vehiculo que esta adelante

                if ($available->available > 0) {//verificar si hay posiciones disponibles
                    for ($i=1; $i <=$available->capacity ; $i++) {
                        if(!$this->parking::find_car_by_position($available->id, $i)){
                            $position   = $i;
                            $data       = ["position" => $position];
                            break;
                        }
                    }
                }else{
                    $data = ["position" => $position];//sino hay posiciones disponibles se establece la del vehiculo que va a salir
                }
                $vehicle->update($data);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
