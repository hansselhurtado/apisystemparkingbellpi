<?php

namespace App\Http\Controllers;

use App\Http\Requests\VehicleRequest;
use App\Http\Resources\Vehicle as ResourcesVehicle;
use App\Http\Resources\VehicleCollection;
use App\Models\Client;
use App\Models\Vehicle;
use App\Models\VehicleType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VehicleController extends ApiController
{

    protected $client;
    protected $vehicle;
    protected $vehicleType;

    public function __construct(Client $client, Vehicle $vehicle, VehicleType $vehicleType){
        $this->client       = $client;
        $this->vehicle      = $vehicle;
        $this->vehicleType  = $vehicleType;
    }

    public function index()
    {
        return $this->successResponse(new VehicleCollection($this->vehicle::all()));
    }

    public function type_vehicle(){
        return $this->successResponse($this->vehicleType::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VehicleRequest $request)
    {
        try {
            DB::beginTransaction();
            $client                 = $request->client;
            $client['id']           = $request->client['document'];
            $vehicle                = $request->vehicle;
            $vehicle['id_client']   = $client['id'];

            //verificamos si existe el cliente
            if(!$this->client::client($client['id'])){
                $client     = $this->client::create($client);
                $vehicle    = $this->vehicle::create($vehicle);
            }else{
                // si el cliente existe, solo guardamos el vehiculo
                $vehicle    = $this->vehicle::create($vehicle);
            }
            DB::commit();
            return $this->successResponse(new ResourcesVehicle($vehicle) ,'El cliente ha sido registrado');

        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 409);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function handle(Request $request, VehicleType $vehicle)
    {
        $vehicle->update([
            "name" =>   $request->name,
            "rate" =>   $request->rate,
            "status" => $request->state
        ]);
        return $this->showMessage("actulizado con exito");
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
