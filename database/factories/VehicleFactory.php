<?php

namespace Database\Factories;

use App\Models\Client;
use App\Models\Vehicle;
use App\Models\VehicleType;
use Illuminate\Database\Eloquent\Factories\Factory;

class VehicleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Vehicle::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $brand = collect(['Suzuki','Yamaha','Honda','Toyota'])->random();
        return [
            'plate'             => $this->faker->unique()->swiftBicNumber(),
            'brand'             => $brand,
            'model'             => $brand.' '.collect(['2020','1990','2000','2010'])->random(),
            'id_client'         => Client::all()->random()->id,
            'id_vehicle_type'   => VehicleType::all()->random()->id,
        ];
    }
}
