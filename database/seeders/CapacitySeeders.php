<?php

namespace Database\Seeders;

use App\Models\Capacity;
use App\Models\VehicleType;
use Illuminate\Database\Seeder;

class CapacitySeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < count(Capacity::CAPACITY) ; $i++) {
            Capacity::create([
                'id_vehicle_type'   => $i + 1,
                'capacity'          => Capacity::CAPACITY[VehicleType::NAME[$i]],
                'available'         => Capacity::CAPACITY[VehicleType::NAME[$i]],
            ]);
        }
    }
}
