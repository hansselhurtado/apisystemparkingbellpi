<?php

namespace Database\Seeders;

use App\Models\Discount;
use Illuminate\Database\Seeder;

class DiscountSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < count(Discount::NAME) ; $i++) {
            Discount::create([
                'name'          =>  Discount::NAME[$i],
                'minutes'       =>  Discount::MINUTES[Discount::NAME[$i]],
                'discount'      =>  Discount::DISCOUNT[Discount::NAME[$i]],
            ]);
        }
    }
}
