<?php

namespace Database\Seeders;

use App\Models\VehicleType;
use Illuminate\Database\Seeder;

class VehicleTypeSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < count(VehicleType::NAME) ; $i++) {
            VehicleType::create([
                'name'  => VehicleType::NAME[$i],
                'rate'  => VehicleType::RATE[VehicleType::NAME[$i]]
            ]);
        }
    }
}
