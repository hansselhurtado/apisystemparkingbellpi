<?php

namespace Database\Seeders;

use App\Models\Discount;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(VehicleTypeSeeders::class);
        $this->call(CapacitySeeders::class);
        $this->call(ClientSeeders::class);
        $this->call(DiscountSeeders::class);
        $this->call(VehicleSeeders::class);
    }
}
