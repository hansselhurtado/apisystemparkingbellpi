<?php

namespace Database\Seeders;

use App\Models\Client;
use Database\Factories\ClientFactory;
use Illuminate\Database\Seeder;

class ClientSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Client::create([
            'id'        =>  1081826503,
            'name'      =>  'Hanssel',
            'last_name' =>  'Hurtado',
        ]);
        Client::factory(10)->create();
    }
}
