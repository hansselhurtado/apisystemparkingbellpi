<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParkingRegister extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capacity', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('id_vehicle_type')->nullable();
            $table->foreign('id_vehicle_type')->references('id')->on('vehicle_types');

            $table->integer('capacity')->comment('capacidad del parqueadero por tipo de vehículo');
            $table->integer('available')->comment('disponibilidad del parqueadero según tipo de vehículo');
            $table->timestamps();
        });

        Schema::create('parking_registers', function (Blueprint $table) {
            $table->increments('id');
            $table->float('amount')->nullable()->comment('valor total que paga el vehículo por tiempo de parqueo');

            $table->unsignedInteger('id_capacity');
            $table->foreign('id_capacity')->references('id')->on('capacity');

            $table->unsignedInteger('id_vehicle');
            $table->foreign('id_vehicle')->references('id')->on('vehicles');

            $table->unsignedInteger('id_discount')->nullable();
            $table->foreign('id_discount')->references('id')->on('discounts');

            $table->integer('position')->comment('posición que tiene el vehículo en el parqueadero');
            $table->enum('status',[0,1])->default(1)->comment('Adentro=1, Salida=0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capacity');
        Schema::dropIfExists('parking_registers');
    }
}
