<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->float('rate')->comment('tarifa por minutos');
            $table->enum('status',[0,1])->default(1);
            $table->timestamps();
        });

        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('plate')->unique();
            $table->string('brand');
            $table->string('model');

            $table->unsignedInteger('id_client');
            $table->foreign('id_client')->references('id')->on('clients');

            $table->unsignedInteger('id_vehicle_type');
            $table->foreign('id_vehicle_type')->references('id')->on('vehicle_types');

            $table->enum('status',[0,1])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_types');
        Schema::dropIfExists('vehicles');
    }
}
