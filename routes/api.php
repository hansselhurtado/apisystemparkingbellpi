<?php

use App\Http\Controllers\DiscountController;
use App\Http\Controllers\ParkingRegisterController;
use App\Http\Controllers\VehicleController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResources([
    'vehicle'        => VehicleController::class,
    'register'       => ParkingRegisterController::class,
    'discount'       => DiscountController::class,
]);

Route::prefix('report')->group(function () {
    Route::post('repeat',       [ParkingRegisterController::class,  'report_repeated']);
    Route::post('vehicle',      [ParkingRegisterController::class,  'report_vehicle']);
    Route::post('amount',       [ParkingRegisterController::class,  'report_mount']);
});
Route::post('handle/discount/{discount}',       [DiscountController::class,  'handle_status']);
Route::get('type/vehicle',                      [VehicleController::class,  'type_vehicle']);

Route::post('handle/vehicle/{vehicle}',         [VehicleController::class,  'handle']);


